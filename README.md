```
fetch('http://localhost:5090/Question/Example1')
```

```
fetch('http://localhost:5090/Question/Example2')
```

```
fetch('http://localhost:5090/Question/Example3')
```

```
fetch('http://localhost:5090/Question/Example4?a=10&b=abc')
```

```
fetch('http://localhost:5090/Question/Example5', {
  method: 'post',
  body: 'p1=1&p2=2',
  headers: {
    'content-type': 'application/x-www-form-urlencoded'
  }
})
```

```
fetch('http://localhost:5090/Question/Example6', {
  method: 'post',
  body: JSON.stringify({p1:'1',p2:'2'}),
  headers: {
    'content-type': 'application/json'
  }
})
```
