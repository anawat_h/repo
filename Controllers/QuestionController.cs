using Microsoft.AspNetCore.Mvc;
using SimpleAPI.ViewModels.Question;

namespace SimpleAPI.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class QuestionController : ControllerBase
    {
        [HttpGet]
        public IActionResult Example1()
        {
            return new ContentResult { ContentType = "text/html", Content = "<h1>Hello World</h1>" };
        }

        [HttpGet]
        public IActionResult Example2()
        {
            return new ContentResult { ContentType = "application/json", Content = "{\"a\":1,\"b\":\"data\"}" };
        }

        [HttpGet]
        public IActionResult Example3()
        {
            return new JsonResult(new { a = 1, b = "data", });
        }

        [HttpGet]
        public IActionResult Example4(int a, string b)
        {
            return new JsonResult(new { a = a, b = b, });
        }

        [HttpPost]
        public IActionResult Example5([FromForm] Example5Params data)
        {
            return new JsonResult(new Example5ViewModel { r1 = data.p1, r2 = data.p2 });
        }

        [HttpPost]
        public IActionResult Example6([FromBody] Example6Params data)
        {
            return new JsonResult(new Example6ViewModel { r1 = data.p1, r2 = data.p2 });
        }
    }
}