﻿namespace SimpleAPI.ViewModels.Question
{
    public class Example6Params
    {
        public string p1 { get; set; }
        public string p2 { get; set; }
    }

    public class Example6ViewModel
    {
        public string r1 { get; set; }
        public string r2 { get; set; }
    }
}
