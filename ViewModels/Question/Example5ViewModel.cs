﻿namespace SimpleAPI.ViewModels.Question
{
    public class Example5Params
    {
        public string p1 { get; set; }
        public string p2 { get; set; }
    }

    public class Example5ViewModel
    {
        public string r1 { get; set; }
        public string r2 { get; set; }
    }
}
